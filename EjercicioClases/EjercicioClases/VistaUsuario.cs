﻿using Ejercicio01_Encapsulacion;
using System;
using System.Collections.Generic;

namespace EjercicioClases
{
    class VistaUsuario
    {
        private ControladorUsuario controlador;
        public VistaUsuario(ControladorUsuario controladorUsuario)
        {
            this.controlador = controladorUsuario;
        }

        public void Menu()
        {

            int opcion;
            do
            {
                Console.WriteLine("\n////////////////////////// MENU //////////////////////////");
                ConsoleColorida.LetterColors(ConsoleColor.Green, "¿ Que opcion quieres ejecutar ?", true);
                Console.WriteLine("1- Mostrar un usuario");
                Console.WriteLine("2- Mostrar todos los usuarios");
                ConsoleColorida.BackgroundColors(ConsoleColor.Blue, "3- Eliminar un usuario", true);
                Console.WriteLine("4- Eliminar todos");
                Console.WriteLine("5- Crear un usuario");
                Console.WriteLine("6- Modificar un usuario");
                ConsoleColorida.LetterColors(ConsoleColor.Red, "0- Salir", true);
                Console.WriteLine("/////////////////////////////////////////////////////////");
                string str_op = Console.ReadLine();
                if (int.TryParse(str_op, out opcion))
                {
                    switch (opcion)
                    {
                        case 1:
                            LeerUno();
                            break;
                        case 2:
                            LeerTodos();
                            break;
                        case 3:
                            EliminarUno();
                            break;
                        case 4:
                            EliminarTodos();
                            break;
                        case 5:
                            AltaMuestra();
                            break;
                        case 6:
                            Modificar();
                            break;
                        case 0:
                            opcion = 0;
                            break;
                        default:
                            Console.WriteLine("Introduzca opcion valida");
                            break;
                    }
                }
                else
                {
                    opcion = -1;
                    Console.WriteLine("Escribe bien el número");
                }
            } while (opcion != 0);
        }

        private bool EliminarUno()
        {
            Console.WriteLine("Busqueda por nombre ");
            string nombre = Console.ReadLine();

            if (controlador.EliminarUno(nombre))
            {
                Console.WriteLine("Al carrer el usuario de nombre " + nombre);
                Persistencia.EliminarUsuario(nombre);
                return true;
            }
            else Console.WriteLine("No hemos encontrado el usuario");

            return false;
        }

        private bool EliminarTodos()
        {
            return controlador.EliminarTodos();
        }

        public void AltaMuestra()
        {

            Console.WriteLine("Nombre del usuario");
            string str = Console.ReadLine();
            Console.WriteLine("Altura del usuario");
            float altura = float.Parse(Console.ReadLine());
            Console.WriteLine("Edad del usuario ");
            int entero = int.Parse(Console.ReadLine());

            Persistencia.AlmacenarUsuarios(new Usuario(str, entero, altura));
            controlador.AltaMuestra(new Usuario(str, entero, altura));
        }
        public void LeerTodos()
        {
            List<Usuario> todos = controlador.LeerTodos();

            if (todos.Count != 0)
            {
                foreach (Usuario usuario in todos)
                {
                    Console.WriteLine(usuario.ToString());
                }
            }
            else Console.WriteLine("\nNo hay usuarios");
        }
        public void LeerUno()
        {
            Console.WriteLine("Introduce el nombre: ");
            string nombre = Console.ReadLine();
            Usuario usuario = controlador.LeerUno(nombre);
            if (usuario != null)
                Console.WriteLine(usuario.ToString());
            else
                Console.WriteLine("No encontrado por " + nombre);
        }

        public void Modificar()
        {
            Console.WriteLine("Intro nombre a buscar: ");
            string nombre = Console.ReadLine();
            if (controlador.LeerUno(nombre) == null)
            {
                Console.WriteLine("No se ha encontrado " + nombre);
            }
            else
            {
                Console.WriteLine("Modifiquemos el usuario!!!");
                Console.WriteLine("Nuevo edad:");
                int entero = int.Parse(Console.ReadLine());
                Console.WriteLine("Nueva altura:");
                float altura = float.Parse(Console.ReadLine());
                Console.WriteLine("Nuevo nombre:");
                string str = Console.ReadLine();
                Usuario aux = controlador.Modificar(controlador.LeerUno(nombre), new Usuario(str, entero, altura));

                if (aux != null)
                    Console.WriteLine("Modificado " + aux.ToString());
                else
                    Console.WriteLine("Usuario no modificado");
            }
        }
    }
}
