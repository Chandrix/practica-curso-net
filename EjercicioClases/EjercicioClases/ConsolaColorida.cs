﻿using System;

namespace EjercicioClases
{
    class ConsoleColorida
    {
        public static void LetterColors(ConsoleColor color, String letras, bool saltoDeLinea)
        {
            Console.ForegroundColor = color;
            if (saltoDeLinea) { Console.WriteLine(letras); }
            else { Console.Write(letras); }
            Console.ResetColor();

        }

        public static void BackgroundColors(ConsoleColor color, string letras, bool saltoDeLinea)
        {
            Console.BackgroundColor = color;
            if (saltoDeLinea) { Console.WriteLine(letras); }
            else { Console.Write(saltoDeLinea); }
            Console.ResetColor();
        }

    }
}
