﻿using Ejercicio01_Encapsulacion;

namespace EjercicioClases
{
    class Program
    {
        IModeloGenerico<Usuario> modelo;
        VistaUsuario vista;

        Program()
        {
            vista = new VistaUsuario(new ControladorUsuario(ModeloUsuario.Instancia));
        }

        static void Main(string[] args)
        {
            Program program = new Program();



            //Persistencia.CargarUsuariosDB(ModeloUsuario.Instancia.Crear(Persistencia.CargarUsuarios()));


            program.vista.Menu();

        }
    }
}
