﻿using System.Collections.Generic;

namespace EjercicioClases
{
    interface IModeloGenerico<Tipo>
    {
        Tipo Crear(Tipo tipo);
        List<Tipo> LeerTodos();
        Tipo LeerUno(string str);
        Tipo Modificar(Tipo tipo, Tipo newTipo);
        bool Eliminar(string str);
        bool EliminarTodos();

    }
}
