﻿namespace Ejercicio01_Encapsulacion
{
    public class Usuario
    {
        private string nombre;
        private int edad;
        private float altura;

        public string Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    nombre = "SIN NOMBRE";
                else
                    nombre = value;
            }
        }
        public int Edad
        {
            get
            {
                return edad;
            }
            set
            {
                edad = value <= 0 ? 1 : value;
            }
        }
        public float Altura
        {
            get
            {
                return altura;
            }
            set
            {
                altura = value <= 0.1f ? 0.1f : value;
            }
        }

        public Usuario(string nombre, int edad, float altura)
        {
            this.Nombre = nombre;
            this.Edad = edad;
            this.Altura = altura;
        }

        public Usuario()
        {
        }

        public override string ToString()
        {
            return $"Usuario de nombre {Nombre} con edad de {edad} años y de altura {Altura}";
        }

        public string GetNombre()
        {
            return Nombre;
        }

        public void SetNombre(string unNombre)
        {
            Nombre = unNombre;
        }
    }
}
