﻿using Ejercicio01_Encapsulacion;
using System;
using System.Collections.Generic;

namespace EjercicioClases
{
    class ModeloUsuario : IModeloGenerico<Usuario>
    {
        public Action<List<Usuario>> AlInicializar;
        public Action<Usuario> AlCrear;
        public Action<Usuario> AlEliminar;
        public Action<Usuario> AlModificar;

        private bool firstCheck;
        private List<Usuario> usuarios;
        private static ModeloUsuario instancia = null;

        public static ModeloUsuario Instancia
        {
            get
            {
                if (instancia == null)
                {
                    instancia = new ModeloUsuario();
                }
                return instancia;
            }
        }

        public List<Usuario> CargarUsuarios(List<Usuario> usuariosLeidos)
        {
            //para cargar datos al iniciar
            foreach (var user in usuariosLeidos)
            {
                usuarios.Add(user);
            }
            return usuarios;
        }

        private ModeloUsuario()
        {
            usuarios = new List<Usuario>();
        }
        public Usuario Crear(Usuario ejemplo)
        {
            // Comprobar si ya se han cargado los datos. Si no se han cargado, y 
            // hay un evento AlInicializar, se llama a este evento, que trae la nueva lista.
            // Una vez se han cargado, ya no se vuelven a cargar

            Checker();

            usuarios.Add(ejemplo);
            return ejemplo;
        }
        public List<Usuario> LeerTodos()
        {
            // Comprobar si ya se han cargado los datos. Si no se han cargado, y 
            // hay un evento AlInicializar, se llama a este evento, que trae la nueva lista.
            // Una vez se han cargado, ya no se vuelven a cargar
            return usuarios;
        }
        public Usuario LeerUno(string str)
        {
            // Comprobar si ya se han cargado los datos. Si no se han cargado, y 
            // hay un evento AlInicializar, se llama a este evento, que trae la nueva lista.
            // Una vez se han cargado, ya no se vuelven a cargar
            foreach (Usuario ejemplo in usuarios)
            {
                if (ejemplo.Nombre == str)
                    return ejemplo;
            }
            return null;
        }
        public Usuario Modificar(Usuario user, Usuario newUser)
        {
            user.Nombre = newUser.Nombre;
            user.Edad = newUser.Edad;
            user.Altura = newUser.Altura;
            return newUser;
        }

        public bool Eliminar(string str)
        {
            Usuario usuario = LeerUno(str);
            if (usuario != null)
            {
                usuarios.Remove(usuario);
                AlEliminar(usuario);
                return true;
            }
            return false;
        }
        public bool EliminarTodos()
        {
            if (usuarios.Count > 0)
            {
                usuarios.Clear();
                return true;
            }
            return false;
        }

        public bool Checker()
        {
            if (firstCheck) AlInicializar(usuarios);
            return true;
        }


    }
}
