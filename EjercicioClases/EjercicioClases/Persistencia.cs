﻿using Ejercicio01_Encapsulacion;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;


namespace EjercicioClases
{
    class Persistencia
    {
        private ControladorUsuario controlador;
        public Persistencia()
        {
            ModeloUsuario.Instancia.AlEliminar = (Usuario user) =>
            {
                EliminarUsuario(user.Nombre);
            };

            ModeloUsuario.Instancia.AlModificar = (Usuario user) =>
            {
                AlmacenarUsuarios(user);
            };

            ModeloUsuario.Instancia.AlEliminar = (Usuario user) =>
            {
                EliminarUsuario(user.Nombre);
            };
            ModeloUsuario.Instancia.AlInicializar = (List<Usuario> listaUsuarios) =>
            {
                CargarUsuariosDB(listaUsuarios);
            };


        }

        public static List<Usuario> CargarUsuarios()
        {
            string json = File.ReadAllText("datosAlmacenados.txt");
            List<Usuario> list = JsonConvert.DeserializeObject<List<Usuario>>(json);
            return list;
        }

        public static bool CargarUsuariosDB(List<Usuario> listaUsuarios)
        {

            foreach (var user in listaUsuarios)
            {

                ModeloUsuario.Instancia.Crear(user);


            }


            return true;
        }



        public static bool AlmacenarUsuarios(Usuario usuario)
        {
            string json = File.ReadAllText("datosAlmacenados.txt");
            var list = JsonConvert.DeserializeObject<List<Usuario>>(json);
            list.Add(usuario);
            var convertedJson = JsonConvert.SerializeObject(list, Formatting.Indented);

            File.WriteAllText("datosAlmacenados.txt", convertedJson);

            return true;
        }
        public static bool EliminarUsuario(string usuario)
        {
            string json = File.ReadAllText("datosAlmacenados.txt");
            var list = JsonConvert.DeserializeObject<List<Usuario>>(json);
            Usuario aux = null;
            foreach (var item in list)
            {
                if (item.Nombre.Equals(usuario)) { aux = item; }
            }
            list.Remove(aux);
            var convertedJson = JsonConvert.SerializeObject(list, Formatting.Indented);
            File.WriteAllText("datosAlmacenados.txt", convertedJson);
            return true;
        }






    }
}
