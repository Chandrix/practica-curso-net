﻿using Ejercicio01_Encapsulacion;
using System.Collections.Generic;

namespace EjercicioClases
{
    class ControladorUsuario
    {
        private IModeloGenerico<Usuario> modelo;

        public ControladorUsuario(IModeloGenerico<Usuario> modelo)
        {
            this.modelo = modelo;
        }
        public Usuario AltaMuestra(Usuario usuario)
        {
            return modelo.Crear(usuario);
        }
        public List<Usuario> LeerTodos()
        {
            return modelo.LeerTodos();
        }
        public Usuario LeerUno(string nombre)
        {
            return modelo.LeerUno(nombre);
        }
        public bool EliminarUno(string nombre)
        {
            return modelo.Eliminar(nombre);
        }
        public bool EliminarTodos()
        {
            return modelo.EliminarTodos();
        }
        public Usuario Modificar(Usuario user, Usuario newUser)
        {
            return modelo.Modificar(user, newUser);
        }
    }
}
